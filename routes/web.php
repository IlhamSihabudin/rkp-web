<?php

use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\PositionController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::group(['middleware' => ['auth:sanctum', 'verified']], function (){
    Route::view('/dashboard', 'dashboard')->name('dashboard');

    Route::resource('department', DepartmentController::class);

    Route::resource('position', PositionController::class);

    Route::resource('user', UserController::class);
    Route::get('user/{user}/change_password', [UserController::class, 'change_password'])->name('user.change_password');
    Route::post('user/{user}/change_password', [UserController::class, 'change_password_store'])->name('user.change_password.store');
    Route::get('user/{user}/digital_signature', [UserController::class, 'digital_signature'])->name('user.digital_signature');
    Route::post('user/{user}/digital_signature', [UserController::class, 'digital_signature_store'])->name('user.digital_signature.store');

    Route::put('user/profile/update-personal-information', [UserController::class, 'updateProfile'])->name('user.update_profile');
    Route::put('user/profile/change-password', [UserController::class, 'updatePersonalPassword'])->name('user.update_password');

    Route::resource('document', DocumentController::class);
    Route::post('document/{document}/approve', [DocumentController::class, 'approve'])->name('document.approve');
    Route::get('document/{document}/pdf', [DocumentController::class, 'exportPdf'])->name('document.pdf');

    Route::get('item/json', [ItemController::class, 'getDataAsJson'])->name('item.getAsJson');
});

Route::view('pdf/view','pages.document.view_pdf');
