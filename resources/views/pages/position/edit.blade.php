@extends('layouts.master')
@section('title', 'Edit Position/Role')

@section('breadcumb')
    <!--begin::Page Title-->
    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Edit</h5>
    <!--end::Page Title-->
    <!--begin::Actions-->
    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        {{--        <li class="breadcrumb-item">--}}
        {{--            <a href="" class="text-muted">Features</a>--}}
        {{--        </li>--}}
        <li class="breadcrumb-item">
            <span class="text-muted">Position/Role</span>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('position.index') }}" class="text-muted">List</a>
        </li>
        <li class="breadcrumb-item">
            <span class="text-muted">Edit</span>
        </li>
    </ul>
    <!--end::Actions-->
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-lg-6 col-md-8 col-sm-10 col-12">
            <div class="card card-custom">
                <div class="card-header flex-wrap">
                    <div class="card-title">
                        <h3 class="card-label">Edit</h3>
                    </div>
                </div>

                <div class="card-body">
                    <form action="{{ route('position.update', $data) }}" method="POST">
                        @csrf @method('PATCH')
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Position Name</label>
                                    <input type="text" class="form-control" placeholder="Name" name="name" value="{{ old('name') ?? $data['name'] }}" required autofocus/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
