@extends('layouts.master')
@section('title', 'Document')

@push('css-style')
    <link href="{{ asset('assets/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('breadcumb')
    <!--begin::Page Title-->
    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Lists</h5>
    <!--end::Page Title-->
    <!--begin::Actions-->
    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
            <a href="" class="text-muted">Document</a>
        </li>
        <li class="breadcrumb-item">
            <span class="text-muted">List</span>
        </li>
    </ul>
    <!--end::Actions-->
@endsection

@section('content')
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">List Document</h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                @role('User')
                <a href="{{ route('document.create') }}" class="btn btn-success font-weight-bolder">
                <span class="svg-icon svg-icon-md">
                    <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <circle fill="#000000" cx="9" cy="15" r="6"/>
                            <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"/>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>Add new
                </a>
                @endrole()
                <!--end::Button-->
            </div>
        </div>

        <div class="card-body">
            <table class="table table-separate table-head-custom table-checkable" id="datatable">
                <thead>
                <tr>
                    <th>No</th>
                    <th>No Document</th>
                    <th>Tanggal Document</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>

        </div>
    </div>
@endsection

@push('js-script')
    <script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#datatable').DataTable({
                scrollX: true,
                processing: true,
                serverSide: true,
                ajax: "{{ route('document.index') }}",
                columns: [
                    {data: null, sortable: false, searchable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {data: 'no_document', name: 'no_document'},
                    {data: 'tgl_document', name: 'tgl_document'},
                    {data: 'status', name: 'status', render(data){
                            if (data === 1){
                                return "<center><div class='badge badge-info'>Waiting Approved by Sect Head</div></center>";
                            }else if (data === 2){
                                return "<center><div class='badge badge-warning'>Waiting Approved by GM/PJO</div></center>";
                            }else if (data === 3){
                                return "<center><div class='badge badge-success'>Document Finised</div></center>";
                            }else{
                                return "<center><div class='badge badge-danger'>Unknown Status</div></center>";
                            }
                        }
                    },
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            })
        });
    </script>
@endpush
