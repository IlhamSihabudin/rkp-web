@extends('layouts.master')
@section('title', 'Add New Document')

@section('breadcumb')
    <!--begin::Page Title-->
    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Add New</h5>
    <!--end::Page Title-->
    <!--begin::Actions-->
    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        {{--        <li class="breadcrumb-item">--}}
        {{--            <a href="" class="text-muted">Features</a>--}}
        {{--        </li>--}}
        <li class="breadcrumb-item">
            <span class="text-muted">Document</span>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('document.index') }}" class="text-muted">List</a>
        </li>
        <li class="breadcrumb-item">
            <span class="text-muted">Add New</span>
        </li>
    </ul>
    <!--end::Actions-->
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card card-custom">
                <div class="card-header flex-wrap">
                    <div class="card-title">
                        <h3 class="card-label">Add New</h3>
                    </div>
                </div>

                <div class="card-body">
                    <form action="{{ route('document.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>No Document</label>
                                    <input type="text" class="form-control" placeholder="No Document" name="no_document" value="{{ old('no_document') }}" required autofocus/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <label>Tanggal Document</label>
                                <div class="input-group date mb-2">
                                    <input type="text" class="form-control" placeholder="Tanggal Document" id="tgl_document" name="tgl_document" value="{{ old('tgl_document') }}" required readonly/>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label>Site</label>
                                    <input type="text" class="form-control" placeholder="Site" name="site" value="{{ old('site') }}" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <label>Items</label>
                                <div id="items-repeater">
                                    <div class="row">
                                        <div data-repeater-list="items-list" class="col-12">
                                            <div data-repeater-item>
                                                <div class="card card-custom card-fit card-border border-success mt-1 mb-1">
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-lg-3">
                                                                <div class="form-group mb-3 mt-3">
                                                                    <div class="typeahead">
                                                                        <input type="text" class="form-control item-typeahead" placeholder="Deskripsi barang" name="item" required/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2">
                                                                <div class="form-group mb-3 mt-3">
                                                                    <input type="text" class="form-control" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" placeholder="Jumlah" name="jumlah" required/>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <div class="form-group mb-3 mt-3 input-group">
                                                                    <div class="input-group-prepend">
                                                                    <span class="input-group-text">
                                                                        Rp
                                                                    </span>
                                                                    </div>
                                                                    <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" class="form-control" placeholder="Harga" name="harga" required/>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-10">
                                                                <div class="form-group mb-3 mt-3">
                                                                    <input type="text" class="form-control" placeholder="Keterangan" name="keterangan"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-auto">
                                                                <a href="javascript:;" data-repeater-delete="" class="btn font-weight-bold btn-danger btn-icon mb-3 mt-3">
                                                                    <i class="la la-remove"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row mt-3">
                                        <div class="col">
                                            <div data-repeater-create="" class="btn font-weight-bold btn-primary">
                                                <i class="la la-plus"></i>
                                                Add Item
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <label>Attachments</label>
                                <div id="attachment-repeater">
                                    <div class="row">
                                        <div data-repeater-list="attachment-list" class="col-12">
                                            <div data-repeater-item>
                                                <div class="card card-custom card-fit card-border border-success mt-1 mb-1">
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-lg-11">
                                                                <div class="form-group mb-3 mt-3">
                                                                    <input type="file" class="form-control" placeholder="Attachment" name="attachment"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-auto">
                                                                <a href="javascript:;" data-repeater-delete="" class="btn font-weight-bold btn-danger btn-icon mb-3 mt-3">
                                                                    <i class="la la-remove"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col">
                                            <div data-repeater-create="" class="btn font-weight-bold btn-primary mt-3">
                                                <i class="la la-plus"></i>
                                                Add Attachment
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js-script')
    <script>
        var arrows;
        if (KTUtil.isRTL()) {
            arrows = {
                leftArrow: '<i class="la la-angle-right"></i>',
                rightArrow: '<i class="la la-angle-left"></i>'
            }
        } else {
            arrows = {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        }

        $(document).ready(function () {
            var bloodhound = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                prefetch: {
                    url : '{{ route('item.getAsJson') }}',
                    cache : false
                }
            });

            $('#tgl_document').datepicker({
                rtl: KTUtil.isRTL(),
                todayHighlight: true,
                format: "yyyy-mm-dd",
                autoclose: true,
                templates: arrows
            });

            $('#items-repeater').repeater({
                show: function() {
                    $(this).slideDown();

                    $('.item-typeahead').typeahead('destroy');
                    $('.item-typeahead').typeahead({
                            hint: true,
                            highlight: true,
                            minLength: 1
                        },
                        {
                            name: 'items',
                            source: bloodhound
                        });
                },

                hide: function(deleteElement) {
                    Swal.fire({
                        title: "Are you sure you want to delete this item?",
                        text: "You won't be able to revert this!",
                        icon: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel!",
                        reverseButtons: true
                    }).then(function(result){
                        if (result.value) {
                            $(this).slideUp(deleteElement);
                        }
                    });
                }
            });

            $('.item-typeahead').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1
                },
                {
                    name: 'items',
                    source: bloodhound
                });

            $('#attachment-repeater').repeater({
                show: function() {
                    $(this).slideDown();
                },

                hide: function(deleteElement) {
                    Swal.fire({
                        title: "Are you sure you want to delete this item?",
                        text: "You won't be able to revert this!",
                        icon: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel!",
                        reverseButtons: true
                    }).then(function(result){
                        if (result.value) {
                            $(this).slideUp(deleteElement);
                        }
                    });
                }
            });
        });
    </script>
@endpush
