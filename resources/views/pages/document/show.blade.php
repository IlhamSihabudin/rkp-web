@extends('layouts.master')
@section('title', 'Detail Document')

@push('css-style')
    <style>
        .display-none{
            display: none;
        }
    </style>
@endpush

@section('breadcumb')
    <!--begin::Page Title-->
    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Detail</h5>
    <!--end::Page Title-->
    <!--begin::Actions-->
    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        {{--        <li class="breadcrumb-item">--}}
        {{--            <a href="" class="text-muted">Features</a>--}}
        {{--        </li>--}}
        <li class="breadcrumb-item">
            <span class="text-muted">Document</span>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('document.index') }}" class="text-muted">List</a>
        </li>
        <li class="breadcrumb-item">
            <span class="text-muted">Detail</span>
        </li>
    </ul>
    <!--end::Actions-->
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card card-custom">
                <div class="card-header flex-wrap">
                    <div class="card-title">
                        <h3 class="card-label">Detail</h3>
                    </div>
                </div>

                <div class="card-body">
                    <form action="{{ route('document.approve', $data) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <h3>No : {{ $data['no_document'] }}</h3>
                            </div>
                        </div>
                        <div class="row ml-3 mr-3 mb-4">
                            <div class="col-md-6 col-12">
                                <h6>Tanggal : {{ date('d M Y', strtotime($data['tgl_document'])) }}</h6>
                            </div>
                            <div class="col-md-6 col-12 text-right">
                                <h6>Site : {{ $data['site'] }}</h6>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-12">
                                <table class="table table-striped" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Deskripsi Barang</th>
                                            <th>Jumlah</th>
                                            <th>Harga</th>
                                            <th>Keterangan</th>
                                            @if(!Auth::user()->hasRole('User') && !Auth::user()->hasRole('Admin') && $data->status != 3)
                                                <th></th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data->document_detail()->where('status', 1)->get() as $detail)
                                            <tr id="row{{ $loop->index+1 }}" class="{{ $detail->status == 0 ? 'bg-danger text-white' : ''}}">
                                                <td>{{ $loop->index+1 }}</td>
                                                <td>{{ $detail->item->item_name }}</td>
                                                <td>{{ $detail->jumlah }}</td>
                                                <td>Rp {{ $detail->harga }}</td>
                                                <td>{{ $detail->keterangan }}</td>
                                                @if(!Auth::user()->hasRole('User') && !Auth::user()->hasRole('Admin') && $data->status != 3)
                                                    <td align="center" width="150">
                                                        <input type="hidden" name="detail_id[]" value="{{ $detail->id }}">
                                                        <input type="hidden" name="status_detail[]" id="status{{ $loop->index+1 }}" value="{{ $detail->status }}">
                                                        <button type="button" onclick="deleteItemRow({{ $loop->index+1 }})" id="btnItemDelete{{ $loop->index+1 }}" class="btn btn-sm font-weight-bolder btn-danger {{ $detail->status == 0 ? "display-none" : ''}}">
                                                            <i class="la la-remove"></i>Delete
                                                        </button>
                                                        <button type="button" onclick="restoreItemRow({{ $loop->index+1 }})" id="btnItemRestore{{ $loop->index+1 }}" class="btn btn-sm font-weight-bolder btn-success {{ $detail->status == 1 ? "display-none" : ''}}">
                                                            <i class="la la-trash-restore"></i>Pulihkan
                                                        </button>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
{{--                                        <tr>--}}
{{--                                            <td colspan="3" align="right">Total</td>--}}
{{--                                            <td colspan="2">Rp {{ $data->document_detail()->sum('harga') }}</td>--}}
{{--                                        </tr>--}}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <label>Attachments</label>
                                <div class="row">
                                    @foreach($data->document_attachment()->where('status', 1)->get() as $attachment)
                                        <div class="col-md-4 col-sm-6 col-12 mb-3 mt-3">
                                            <div id="rowAttach{{ $loop->index+1 }}" class="card card-custom card-fit {{ $attachment->status == 0 ? 'bg-danger' : 'card-border border-success' }} mt-1 mb-1">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="row">
                                                                <div class="col-9">
                                                                    <input type="text" class="form-control" value="{{ $attachment->file }}" disabled>
                                                                </div>
                                                                <div class="col-3 text-center">
                                                                    @if(!Auth::user()->hasRole('User') && !Auth::user()->hasRole('Admin') && $data->status != 3)
                                                                        <input type="hidden" name="attach_id[]" value="{{ $attachment->id }}">
                                                                        <input type="hidden" name="status_attach[]" id="statusAttach{{ $loop->index+1 }}" value="{{ $attachment->status }}">
                                                                        <span class="{{ $attachment->status == 0 ? "display-none" : ''}}" id="btnAttachDelete{{ $loop->index+1 }}">
                                                                        <button type="button" onclick="deleteAttachRow({{ $loop->index+1 }})" class="btn font-weight-bold btn-danger btn-icon">
                                                                                <i class="la la-remove"></i>
                                                                            </button>
                                                                        </span>
                                                                        <span class="{{ $attachment->status == 1 ? "display-none" : ''}}" id="btnAttachRestore{{ $loop->index+1 }}">
                                                                        <button type="button" onclick="restoreAttachRow({{ $loop->index+1 }})" class="btn font-weight-bold btn-success btn-icon">
                                                                            <i class="la la-trash-restore"></i>
                                                                        </button>
                                                                    </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="row mt-5">
                                                                <div class="col-12 text-center">
                                                                    <a href="{{ asset('storage/document-attachment/'.$attachment->file) }}" target="_blank">
                                                                        <div class="card card-custom card-fit bg-success mt-1 mb-1">
                                                                            <div class="card-body">
                                                                                <i class="fas fa-file-alt fa-4x text-white"></i>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @if(!Auth::user()->hasRole('User') && !Auth::user()->hasRole('Admin') && $data->status != 3)
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-success btn-block">Update</button>
                                </div>
                            </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js-script')
    <script>
        function deleteItemRow(row){
            $('#row'+row).addClass('bg-danger text-white');
            $('#btnItemDelete'+row).hide();
            $('#btnItemRestore'+row).show();
            $('#status'+row).val(0);
        }

        function restoreItemRow(row) {
            $('#row'+row).removeClass('bg-danger text-white');
            $('#btnItemDelete'+row).show();
            $('#btnItemRestore'+row).hide();
            $('#status'+row).val(1);
        }

        function deleteAttachRow(row){
            $('#rowAttach'+row).addClass('bg-danger');
            $('#rowAttach'+row).removeClass('card-border border-success');
            $('#btnAttachDelete'+row).hide();
            $('#btnAttachRestore'+row).show();
            $('#statusAttach'+row).val(0);
        }

        function restoreAttachRow(row) {
            $('#rowAttach'+row).removeClass('bg-danger');
            $('#rowAttach'+row).addClass('card-border border-success');
            $('#btnAttachDelete'+row).show();
            $('#btnAttachRestore'+row).hide();
            $('#statusAttach'+row).val(1);
        }
    </script>
@endpush
