<!doctype html>
<html lang="en">
<head>
{{--    <title>Document PDF - {{ $document['no_document'] }}</title>--}}

    <style>
        body{
            font-size: 11px;
        }

        .container{
            margin: 10px;
            padding: 0 20px 20px 20px;
            border: 1px solid black;
        }

        table, tr, td {
            border-collapse: collapse;
        }

        .mt-1{
            margin-top: 10px;
            margin-bottom: 20px;
        }
    </style>
</head>
<body>
    <table width="100%">
        <tr>
            <td>
                <div class="container">
                    <img src="{{ public_path('assets/media/PPA.png') }}" class="mt-1" alt="PPA" width="50px">
                    <table width="100%" border="1">
                        <tr align="center">
                            <td colspan="5"><b style="font-size: 12px">RENCANA KEBUTUHAN BARANG</b></td>
                        </tr>
                        <tr>
                            <td colspan="5" style="padding-bottom: 10px">
                                <table width="100%">
                                    <tr align="center">
                                        <td colspan="2" style="margin-bottom: 10px">NO : {{ $document['no_document'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Tanggal : {{ date('d F Y', strtotime($document['tgl_document'])) }}
                                        </td>
                                        <td width="100">
                                            Site : {{ $document['site'] }}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr align="center">
                            <td width="15">NO.</td>
                            <td>DESKRPSI BARANG</td>
                            <td>JUMLAH</td>
                            <td>HARGA</td>
                            <td>KETERANGAN</td>
                        </tr>
                        @foreach($document_detail as $detail)
                            <tr>
                                <td align="center">{{ $loop->index+1 }}</td>
                                <td>{{ $detail['item']['item_name'] }}</td>
                                <td>{{ $detail['jumlah'] }}</td>
                                <td>Rp {{ $detail['harga'] }}</td>
                                <td>{{ $detail['keterangan'] }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="3" align="right">Total</td>
                            <td colspan="2">Rp {{ $document_detail_total }}</td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 10px" colspan="5"></td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                Keterangan :
                                <table width="50%">
                                    @foreach($document_attachment as $attach)
                                        <tr>
                                            <td style="padding-left: 20px">
                                                <img src="{{ public_path('storage/document-attachment/'.$attach['file']) }}" class="mt-1" alt="{{ $attach['file'] }}" width="100%">
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <table width="100%">
                                    <tr align="center">
                                        <td>Dibuat oleh,</td>
                                        <td>Diketahui oleh,</td>
                                        <td>Disetujui oleh,</td>
                                        <td>Diterima oleh,</td>
                                    </tr>
                                    <tr align="center">
                                        <td style="padding: 15px 0">
                                            Created by {{ $user_created }} <br>
                                            on {{ date('d-M-Y H:i:s', strtotime($document['created_at'])) }}
                                        </td>
                                        <td style="padding: 15px 0">
                                            Known by {{ $document_approve['sect_head']['name'] }} <br>
                                            on {{ date('d-M-Y H:i:s', strtotime($document_approve['sect_head_tgl'])) }}
                                        </td>
                                        <td style="padding: 15px 0">
                                            Approved by {{ $document_approve['gm']['name'] }} <br>
                                            on {{ date('d-M-Y H:i:s', strtotime($document_approve['gm_tgl'])) }}
                                        </td>
                                        <td style="padding: 15px 0"></td>
                                    </tr>
                                    <tr align="center">
                                        <td>User</td>
                                        <td>Sect. Head {{ $department }}</td>
                                        <td>PJO</td>
                                        <td>Logistik</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</body>
</html>
