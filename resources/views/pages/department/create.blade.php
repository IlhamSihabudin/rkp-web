@extends('layouts.master')
@section('title', 'Add New Department')

@section('breadcumb')
    <!--begin::Page Title-->
    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Add New</h5>
    <!--end::Page Title-->
    <!--begin::Actions-->
    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        {{--        <li class="breadcrumb-item">--}}
        {{--            <a href="" class="text-muted">Features</a>--}}
        {{--        </li>--}}
        <li class="breadcrumb-item">
            <span class="text-muted">Department</span>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('department.index') }}" class="text-muted">List</a>
        </li>
        <li class="breadcrumb-item">
            <span class="text-muted">Add New</span>
        </li>
    </ul>
    <!--end::Actions-->
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-lg-6 col-md-8 col-sm-10 col-12">
            <div class="card card-custom">
                <div class="card-header flex-wrap">
                    <div class="card-title">
                        <h3 class="card-label">Add New</h3>
                    </div>
                </div>

                <div class="card-body">
                    <form action="{{ route('department.store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Department Name</label>
                                    <input type="text" class="form-control" placeholder="Name" name="name" value="{{ old('name') }}" required autofocus/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Status</label>
                                    <select name="status" id="status" class="form-control" required>
                                        <option value=""></option>
                                        <option value="1" {{ old('status') == '1' ? 'selected' : '' }}>Aktif</option>
                                        <option value="0" {{ old('status') == '0' ? 'selected' : '' }}>Non Aktif</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js-script')
    <script>
        $(document).ready(function () {
            $('#status').select2({
                placeholder: "Select a status",
                allowClear: true
            });
        });
    </script>
@endpush
