@extends('layouts.master')
@section('title', 'Upload Digital Signature User')

@section('breadcumb')
    <!--begin::Page Title-->
    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Upload Digital Signature</h5>
    <!--end::Page Title-->
    <!--begin::Actions-->
    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
            <span class="text-muted">User</span>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('user.index') }}" class="text-muted">List</a>
        </li>
        <li class="breadcrumb-item">
            <span class="text-muted">Upload Digital Signature</span>
        </li>
    </ul>
    <!--end::Actions-->
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8 col-sm-10 col-12">
            <div class="card card-custom">
                <div class="card-header flex-wrap">
                    <div class="card-title">
                        <h3 class="card-label">Upload Digital Signature</h3>
                    </div>
                </div>

                <div class="card-body">
                    <form action="{{ route('user.digital_signature.store', $data) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Upload New Digital Signature</label>
                                    <input type="file" accept="image/*" class="form-control" placeholder="Digital Signature" name="digital_signature" id="digital_signature" required autofocus/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 text-center">
                                <div class="form-group">
                                    <img src="{{ $data->digital_signature ? asset('storage/user/'.$data->digital_signature) : asset('assets/media/no-image.jpeg') }}" width="200px" alt="{{ $data->digital_signature ?? 'no image' }}" id="preview_image">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block">Upload</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js-script')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview_image').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#digital_signature").change(function(){
            readURL(this);
        });
    </script>
@endpush
