@extends('layouts.master')
@section('title', 'Change Password User')

@section('breadcumb')
    <!--begin::Page Title-->
    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Change Password</h5>
    <!--end::Page Title-->
    <!--begin::Actions-->
    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
            <span class="text-muted">User</span>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('user.index') }}" class="text-muted">List</a>
        </li>
        <li class="breadcrumb-item">
            <span class="text-muted">Change Password</span>
        </li>
    </ul>
    <!--end::Actions-->
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8 col-sm-10 col-12">
            <div class="card card-custom">
                <div class="card-header flex-wrap">
                    <div class="card-title">
                        <h3 class="card-label">Change Password</h3>
                    </div>
                </div>

                <div class="card-body">
                    <form action="{{ route('user.change_password.store', $data) }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>New Password</label>
                                    <input type="password" class="form-control" placeholder="Password" name="password" value="" autocomplete="new-password" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation" value="" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-block">Change Password</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
