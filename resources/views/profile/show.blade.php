@extends('layouts.master')
@section('title', 'Pengguna')

@section('breadcumb')
    <!--begin::Page Title-->
    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Pengguna</h5>
    <!--end::Page Title-->
    <!--begin::Actions-->
    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        {{--        <li class="breadcrumb-item">--}}
        {{--            <a href="" class="text-muted">Features</a>--}}
        {{--        </li>--}}
        <li class="breadcrumb-item">
            <span class="text-muted">Users Management</span>
        </li>
        <li class="breadcrumb-item">
            <span class="text-muted">Pengguna</span>
        </li>
    </ul>
    <!--end::Actions-->
@endsection

@section('content')
    <div class="row justify-content-center mb-4">
        <div class="col-md-8">
            <form action="{{ route('user.update_profile') }}" method="POST">
                @csrf @method('PUT')
                <div class="card card-custom card-stretch">
                    <!--begin::Header-->
                    <div class="card-header py-3">
                        <div class="card-title align-items-start flex-column">
                            <h3 class="card-label font-weight-bolder text-dark">Personal Information</h3>
                            <span class="text-muted font-weight-bold font-size-sm mt-1">Update your personal informaiton</span>
                        </div>
                        <div class="card-toolbar">
                            <button type="submit" class="btn btn-success mr-2">Save Changes</button>
                        </div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Form-->
                    <form class="form">
                        <!--begin::Body-->
                        <div class="card-body">
                            <div class="form-group">
                                <label class="form-label">Nama Pengguna</label>
                                <input class="form-control" type="text" name="name" value="{{ Auth::user()->name }}" />
                            </div>
                            <div class="form-group">
                                <label class="form-label">Email</label>
                                <input class="form-control" type="email" name="email" value="{{ Auth::user()->email }}" />
                            </div>
                        </div>
                        <!--end::Body-->
                    </form>
                    <!--end::Form-->
                </div>
            </form>
        </div>
    </div>
    <div class="row justify-content-center mb-4">
        <div class="col-md-8">
            <form action="{{ route('user.update_password') }}" method="POST">
                @csrf @method('PUT')
                <div class="card card-custom card-stretch">
                    <!--begin::Header-->
                    <div class="card-header py-3">
                        <div class="card-title align-items-start flex-column">
                            <h3 class="card-label font-weight-bolder text-dark">Change Password</h3>
                            <span class="text-muted font-weight-bold font-size-sm mt-1">Update your password</span>
                        </div>
                        <div class="card-toolbar">
                            <button type="submit" class="btn btn-success mr-2">Save Changes</button>
                        </div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Form-->
                    <form class="form">
                        <!--begin::Body-->
                        <div class="card-body">
                            <div class="form-group">
                                <label class="form-label">Current Password</label>
                                <input class="form-control" type="password" name="current_password" />
                            </div>
                            <div class="form-group">
                                <label class="form-label">New Password</label>
                                <input class="form-control" type="password" name="password" />
                            </div>
                            <div class="form-group">
                                <label class="form-label">Confirm New Password</label>
                                <input class="form-control" type="password" name="password_confirmation" />
                            </div>
                        </div>
                        <!--end::Body-->
                    </form>
                    <!--end::Form-->
                </div>
            </form>
        </div>
    </div>
@endsection
