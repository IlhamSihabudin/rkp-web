@extends('layouts.master')
@section('title', 'Dashboard')

@section('breadcumb')
    <!--begin::Page Title-->
    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Dashboard</h5>
    <!--end::Page Title-->
    <!--begin::Actions-->
    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        {{--        <li class="breadcrumb-item">--}}
        {{--            <a href="" class="text-muted">Features</a>--}}
        {{--        </li>--}}
        <li class="breadcrumb-item">
            <span class="text-muted">Dashboard</span>
        </li>
    </ul>
    <!--end::Actions-->
@endsection

@section('content')
    Dashboard
@endsection
