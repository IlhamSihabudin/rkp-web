<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\Item;
use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Item::create([
            'item_name' => 'Laptop',
            'department_id' => Department::first()->id
        ]);

        Item::create([
            'item_name' => 'Komputer',
            'department_id' => Department::first()->id
        ]);

        Item::create([
            'item_name' => 'Handphone',
            'department_id' => Department::first()->id
        ]);
    }
}
