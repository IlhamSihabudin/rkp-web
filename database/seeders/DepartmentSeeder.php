<?php

namespace Database\Seeders;

use App\Models\Department;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::create([
            'name' => 'Production',
            'status' => '1',
        ]);

        Department::create([
            'name' => 'Engineer',
            'status' => '1',
        ]);
    }
}
