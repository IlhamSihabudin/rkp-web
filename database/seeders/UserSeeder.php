<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('admin123'),
            'department_id' => Department::first()->id
        ]);

        $user->assignRole('Admin');

        $user = User::create([
            'name' => 'Ilham Sihabudin',
            'email' => 'ilham@gmail.com',
            'password' => Hash::make('ilham123'),
            'department_id' => Department::first()->id
        ]);

        $user->assignRole('User');

        $user = User::create([
            'name' => 'Sect Head',
            'email' => 'secthead@gmail.com',
            'password' => Hash::make('secthead123'),
            'department_id' => Department::first()->id
        ]);

        $user->assignRole('Sect Head');

        $user = User::create([
            'name' => 'GM',
            'email' => 'gm@gmail.com',
            'password' => Hash::make('gm123456'),
            'department_id' => Department::first()->id
        ]);

        $user->assignRole('GM');
    }
}
