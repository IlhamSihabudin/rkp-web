<?php

namespace App\Repositories;

use App\Models\DocumentApproval;

class DocumentApprovalRepository
{
    public function insert($data)
    {
        return DocumentApproval::create($data);
    }

    public function setApprove($document_id, $data)
    {
        return DocumentApproval::where('document_id', $document_id)->update($data);
    }
}
