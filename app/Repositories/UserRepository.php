<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserRepository
{
    public function getQueryAll()
    {
        return User::query()
            ->leftJoin('departments', 'departments.id', 'department_id')
            ->where('users.id', '!=', Auth::id())
            ->select('users.*', 'departments.name as department_name');
    }

    public function insert(array $data)
    {
        unset($data['password_confirmation']);
        $data['password'] = Hash::make($data['password']);
        $data['created_by'] = Auth::id();
        return User::create($data);
    }

    public function update(array $data, User $user)
    {
        $data['updated_by'] = Auth::id();
        return $user->update($data);
    }

    public function delete(User $user)
    {
        $user->deleteProfilePhoto();
        $user->tokens->each->delete();
        $user->delete();
    }

    public function changePassword(array $data, User $user)
    {
        unset($data['password_confirmation']);
        $data['password'] = Hash::make($data['password']);
        $data['updated_by'] = Auth::id();
        return $user->update($data);
    }
}
