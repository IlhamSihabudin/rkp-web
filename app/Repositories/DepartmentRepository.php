<?php

namespace App\Repositories;

use App\Models\Department;
use Illuminate\Support\Facades\Auth;

class DepartmentRepository
{
    public function getQueryAll()
    {
        return Department::query();
    }

    public function insert(array $data)
    {
        $data['created_by'] = Auth::id();
        return Department::create($data);
    }

    public function update(array $data, Department $department)
    {
        $data['updated_by'] = Auth::id();
        return $department->update($data);
    }

    public function delete(Department $department)
    {
        $department->delete();
    }
}
