<?php

namespace App\Repositories;

use App\Models\Role;
use Illuminate\Support\Facades\Auth;

class PositionRepository
{
    public function getQueryAll()
    {
        return Role::query();
    }

    public function getDataById($id)
    {
        return Role::where('id', $id)->first();
    }

    public function update(array $data, Role $position)
    {
        return $position->update($data);
    }
}
