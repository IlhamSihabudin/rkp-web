<?php

namespace App\Repositories;

use App\Models\Item;
use Illuminate\Support\Facades\Auth;

class ItemRepository
{
    public function getAllPluckName()
    {
        return Item::where('department_id', Auth::user()->department_id)->get()->pluck('item_name');
    }

    public function checkItem($item_name)
    {
        $item = Item::firstOrCreate([
            'item_name' => $item_name,
            'department_id' => Auth::user()->department_id
        ], [
            'created_by' => Auth::id()
        ]);

        return $item->id;
    }
}
