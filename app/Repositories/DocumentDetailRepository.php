<?php

namespace App\Repositories;

use App\Models\DocumentDetail;

class DocumentDetailRepository
{
    public function multipleInsert(array $datas)
    {
        DocumentDetail::insert($datas);
    }

    public function updateStatus($id, $status){
        DocumentDetail::where('id', $id)->update(['status' => $status]);
    }
}
