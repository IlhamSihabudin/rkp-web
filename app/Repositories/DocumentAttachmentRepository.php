<?php

namespace App\Repositories;

use App\Models\DocumentAttachment;

class DocumentAttachmentRepository
{
    public function multipleInsert(array $datas)
    {
        DocumentAttachment::insert($datas);
    }

    public function updateStatus($id, $status){
        DocumentAttachment::where('id', $id)->update(['status' => $status]);
    }
}
