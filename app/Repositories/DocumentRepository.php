<?php

namespace App\Repositories;

use App\Models\Document;
use Illuminate\Support\Facades\Auth;

class DocumentRepository
{
    public function getQueryAll()
    {

        $data = Document::query()
            ->where(function ($query){
                if (Auth::user()->hasRole('Sect Head')){
                    $query->where('status', 1);
                }elseif (Auth::user()->hasRole('GM')){
                    $query->where('status', 2);
                }

                if (!Auth::user()->hasRole('User')){
                    $query->orWhere('status', 3);
                }
            });

        if (!Auth::user()->hasRole('GM') && !Auth::user()->hasRole('Admin')) {
            $data->where('department_id', Auth::user()->department_id);
        }

        if (Auth::user()->hasRole('User')){
            $data->where('created_by', Auth::id());
        }

        return $data;
    }

    public function insert(array $data)
    {
        $data['created_by'] = Auth::id();
        return Document::create($data);
    }

    public function update(array $data, Document $document)
    {
        $data['updated_by'] = Auth::id();
        return $document->update($data);
    }

    public function delete(Document $document)
    {
        $document->delete();
    }

    public function updateStatus(Document $document)
    {
        $document->update(['status' => $document->status + 1]);
    }
}
