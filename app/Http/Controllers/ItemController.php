<?php

namespace App\Http\Controllers;

use App\Services\ItemService;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    private $itemService;

    public function __construct()
    {
        $this->itemService = new ItemService();
    }

    public function getDataAsJson()
    {
        return $this->itemService->getALlAsJson();
    }
}
