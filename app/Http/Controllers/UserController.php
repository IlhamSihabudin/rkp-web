<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Repositories\DepartmentRepository;
use App\Repositories\UserRepository;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    protected $userService, $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->userService = new UserService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()){
            return $this->userService->getDatatables();
        }

        return view('pages.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.user.create', $this->userService->showPositionAndDepartment());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only([
            'name',
            'email',
            'password',
            'password_confirmation',
            'department_id',
            'position_id'
        ]);

        try {
            if ($request->hasFile('digital_signature')){
                $data['digital_signature'] = $this->userService->uploadDigitalSignature($request->only(['digital_signature']), $request->file('digital_signature')->getClientOriginalExtension());
            }else{
                $data['digital_signature'] = '';
            }

            $this->userService->saveData($data);
        }catch (\Exception $e){
            return back()->withErrors($e->getMessage())->withInput();
        }

        return redirect()->route('user.index')->withMessage('Add user successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|void
     */
    public function edit(User $user)
    {
        return view('pages.user.edit', array_merge(['data' => $user], $this->userService->showPositionAndDepartment()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $data = $request->only([
            'name',
            'email',
            'department_id',
            'position_id'
        ]);

        try {
            $this->userService->updateData($data, $user);
        }catch (\Exception $e){
            return back()->withErrors($e->getMessage())->withInput();
        }

        return redirect()->route('user.index')->withMessage('Update user successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(User $user)
    {
        try {
            $this->userRepository->delete($user);
        }catch (\Exception $e){
            return back()->withErrors($e->getMessage());
        }

        return back()->withMessage('Delete user successfully');
    }

    public function change_password(User $user)
    {
        return view('pages.user.change_password',[
            'data' => $user
        ]);
    }

    public function change_password_store(Request $request, User $user)
    {
        $data = $request->only([
            'password',
            'password_confirmation',
        ]);

        try {
            $this->userService->changePasswordUser($data, $user);
        }catch (\Exception $e){
            return back()->withErrors($e->getMessage())->withInput();
        }

        return redirect()->route('user.index')->withMessage('Change password successfully');
    }

    public function digital_signature(User $user)
    {
        return view('pages.user.digital_signature',[
            'data' => $user
        ]);
    }

    public function digital_signature_store(Request $request, User $user)
    {
        $data = [];

        try {
            if ($request->hasFile('digital_signature')){
                $data['digital_signature'] = $this->userService->updateDigitalSignature($request->only(['digital_signature']), $request->file('digital_signature')->getClientOriginalExtension(), $user);
            }else{
                $data['digital_signature'] = '';
            }

            $this->userService->updateData($data, $user);
        }catch (\Exception $e){
            return back()->withErrors($e->getMessage())->withInput();
        }

        return redirect()->route('user.index')->withMessage('Upload digital signature successfully');
    }

    public function updateProfile(Request $request)
    {
        try {
            $this->userService->updatePersonalData($request->except('_token'), Auth::user());
        }catch (\Exception $e){
            return back()->withErrors($e->getMessage())->withInput();
        }

        return back()->withMessage('Update personal information successfully');
    }

    public function updatePersonalPassword(Request $request)
    {
        try {
            $this->userService->changePersonalPasswordUser($request->except('_token'), Auth::user());
        }catch (\Exception $e){
            return back()->withErrors($e->getMessage())->withInput();
        }

        return redirect()->route('profile.show')->withMessage('Update personal information successfully');
    }
}
