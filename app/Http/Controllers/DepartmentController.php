<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Repositories\DepartmentRepository;
use App\Services\DepartmentService;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    protected $departmentService, $departmentRepository;

    public function __construct()
    {
        $this->departmentRepository = new DepartmentRepository();
        $this->departmentService = new DepartmentService();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        if ($request->ajax()){
            return $this->departmentService->getDatatables();
        }

        return view('pages.department.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->only([
            'name',
            'status'
        ]);

        try {
            $this->departmentService->saveData($data);
        }catch (\Exception $e){
            return back()->withErrors($e->getMessage())->withInput();
        }

        return redirect()->route('department.index')->withMessage('Add department successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Department $department
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Department $department)
    {
        return view('pages.department.edit', [
            'data' => $department
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Department $department
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        $data = $request->only([
            'name',
            'status'
        ]);

        try {
            $this->departmentService->updateData($data, $department);
        }catch (\Exception $e){
            return back()->withErrors($e->getMessage())->withInput();
        }

        return redirect()->route('department.index')->withMessage('Update department successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        try {
            $this->departmentRepository->delete($department);
        }catch (\Exception $e){
            return back()->withErrors($e->getMessage());
        }

        return back()->withMessage('Delete department successfully');
    }
}
