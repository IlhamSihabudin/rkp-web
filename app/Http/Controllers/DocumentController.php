<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Repositories\DocumentRepository;
use App\Services\DocumentService;
use PDF;
use Illuminate\Http\Request;

class DocumentController extends Controller
{
    protected $documentService, $documentRepository;

    public function __construct()
    {
        $this->documentRepository = new DocumentRepository();
        $this->documentService = new DocumentService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()){
            return $this->documentService->getDatatables();
        }

        return view('pages.document.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.document.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->documentService->saveData($request->except('_token'));
        }catch (\Exception $e){
            return back()->withErrors($e->getMessage())->withInput();
        }

        return redirect()->route('document.index')->withMessage('Add document successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function show(Document $document)
    {
        return view('pages.document.show', [
            'data' => $document
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function edit(Document $document)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Document $document)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function destroy(Document $document)
    {
        //
    }

    public function approve(Request $request, Document $document)
    {
        try {
            $this->documentService->approveDocument($request->except('_token'), $document);
        }catch (\Exception $e){
            return back()->withErrors($e->getMessage())->withInput();
        }

        return redirect()->route('document.index')->withMessage('Approve document successfully');
    }

    public function exportPdf(Document $document)
    {
//        dd($document->with('document_detail')->toArray());
        $data = [
            'document' => $document->toArray(),
            'user_created' => $document->user_created->name,
            'department' => $document->department->name,
            'document_detail' => $document->document_detail()->where('status', 1)->with('item')->get()->toArray(),
            'document_detail_total' => $document->document_detail()->where('status', 1)->sum('harga'),
            'document_attachment' => $document->document_attachment()->where('status', 1)->where(function ($query) {
                $query->where('file', 'LIKE', '%.png')
                    ->orWhere('file', 'LIKE', '%.jpg')
                    ->orWhere('file', 'LIKE', '%.jpeg')
                    ->orWhere('file', 'LIKE', '%.gif');
            })->get()->toArray(),
            'document_approve'  => $document->document_approval()->with(['sect_head', 'gm'])->first()->toArray()
        ];

//        dd($data);

        $pdf = PDF::loadView('pages.document.view_pdf', $data);

        return $pdf->download($document->no_document.'.pdf');
    }
}
