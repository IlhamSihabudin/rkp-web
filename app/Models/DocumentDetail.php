<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DocumentDetail extends Model
{
    use Uuids;
    use HasFactory;

    protected $fillable = [
        'id',
        'document_id',
        'item_id',
        'harga',
        'keterangan',
        'status'
    ];

    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id', 'id');
    }
}
