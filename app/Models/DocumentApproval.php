<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DocumentApproval extends Model
{
    use Uuids;
    use HasFactory;

    protected $fillable = [
        'id',
        'document_id',
        'sect_head_id',
        'sect_head_tgl',
        'gm_id',
        'gm_tgl',
    ];

    public function sect_head()
    {
        return $this->belongsTo(User::class, 'sect_head_id', 'id');
    }

    public function gm()
    {
        return $this->belongsTo(User::class, 'gm_id', 'id');
    }
}
