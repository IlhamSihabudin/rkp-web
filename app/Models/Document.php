<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    use Uuids;
    use HasFactory;

    protected $fillable = [
        'id',
        'no_document',
        'tgl_document',
        'site',
        'status',
        'department_id',
        'created_by',
        'updated_by'
    ];

    public function document_detail()
    {
        return $this->hasMany(DocumentDetail::class, 'document_id', 'id');
    }

    public function document_attachment()
    {
        return $this->hasMany(DocumentAttachment::class, 'document_id', 'id');
    }

    public function document_approval()
    {
        return $this->hasMany(DocumentApproval::class, 'document_id', 'id');
    }

    public function user_created()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }
}
