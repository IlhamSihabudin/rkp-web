<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DocumentAttachment extends Model
{
    use Uuids;
    use HasFactory;

    protected $fillable = [
        'id',
        'document_id',
        'file',
        'status'
    ];
}
