<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use Uuids;
    use HasFactory;

    protected $fillable = [
        'id',
        'item_name',
        'department_id',
        'created_by',
        'updated_by'
    ];
}
