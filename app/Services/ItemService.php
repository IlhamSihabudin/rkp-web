<?php

namespace App\Services;

use App\Repositories\ItemRepository;
use Illuminate\Support\Facades\Auth;

class ItemService
{
    private $itemRepository;

    public function __construct()
    {
        $this->itemRepository = new ItemRepository();
    }

    public function getALlAsJson()
    {
        if (Auth::user()){
            $data = $this->itemRepository->getAllPluckName()->toArray();
        }else{
            $data = [];
        }
        return $data;
    }
}
