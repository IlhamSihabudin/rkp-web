<?php

namespace App\Services;

use App\Models\Department;
use App\Models\User;
use App\Repositories\DepartmentRepository;
use App\Repositories\PositionRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;
use Laravel\Jetstream\Jetstream;
use Yajra\DataTables\DataTables;

class UserService
{
    protected $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    public function insertValidation(array $data)
    {
        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'position_id' => ['required', 'string', 'max:255'],
            'department_id' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }
    }

    public function updateValidation(array $data, $id)
    {
        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,id,'.$id],
            'position_id' => ['required', 'string', 'max:255'],
            'department_id' => ['required', 'string', 'max:255'],
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }
    }

    public function getDatatables()
    {
        $query = $this->userRepository->getQueryAll();

        return DataTables::of($query)
            ->editColumn('action', function ($model){
//                <a href='".route('user.digital_signature', $model['id'])."' class='btn btn-sm btn-light-info btn-circle btn-icon mr-2' title='Upload Digital Signature'><i class='fas fa-signature icon-nm'></i></a>
                $response = "
                    <div class='text-center'>
                        <a href='".route('user.edit', $model['id'])."' class='btn btn-sm btn-light-success btn-circle btn-icon mr-2' title='Edit'><i class='fas fa-pen icon-nm'></i></a>
                        <a href='".route('user.change_password', $model['id'])."' class='btn btn-sm btn-light-warning btn-circle btn-icon mr-2' title='Change Password'><i class='fas fa-key icon-nm'></i></a>
                        <a href='".route('user.destroy', $model['id'])."' class='btn btn-sm btn-light-danger btn-circle btn-icon mr-2 btn-delete' title='Delete'><i class='fas fa-trash-alt icon-nm'></i></a>
                    </div>";

                return $response;
            })
            ->make(true);
    }

    public function showPositionAndDepartment()
    {
        $positionRepository = new PositionRepository();
        $departmentRepository = new DepartmentRepository();

        return [
            'positions' => $positionRepository->getQueryAll()->get(),
            'departments' => $departmentRepository->getQueryAll()->get()
        ];
    }

    public function saveData(array $data)
    {
        $this->insertValidation($data);
        $position = $data['position_id'];
        unset($data['position_id']);

        $user = $this->userRepository->insert($data);

        $user->assignRole($position);
    }

    public function updateData($data, User $user)
    {
        if (!isset($data['digital_signature'])){
            $this->updateValidation($data, $user->id);

            $position = $data['position_id'];
            unset($data['position_id']);
        }

        $this->userRepository->update($data, $user);

        if (!isset($data['digital_signature'])){
            $user->syncRoles($position);
        }
    }

    public function changePasswordUser($data, User $user)
    {
        $validator = Validator::make($data, [
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        $this->userRepository->changePassword($data, $user);
    }

    public function updatePersonalData($data, $user)
    {
        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,id,'.$user->id],
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        $this->userRepository->update($data, $user);
    }

    public function changePersonalPasswordUser($data, User $user)
    {
        $validator = Validator::make($data, [
            'current_password' => ['required', 'string'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        if (!Hash::check($data['current_password'], $user->password)){
            throw new InvalidArgumentException('The current password is incorrect');
        }

        unset($data['current_password']);
        $this->userRepository->changePassword($data, $user);
    }

    public function uploadDigitalSignature($data, $ext)
    {
        $validator = Validator::make($data, [
            'digital_signature' => 'required|mimes:jpeg,jpg,png,gif',
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        $filename = 'user_'.date('YmdHis').rand(0001, 9999).'.'.$ext;
        Storage::disk('public')->putFileAs('user', $data['digital_signature'], $filename);

        return $filename;
    }

    public function updateDigitalSignature($data, $ext, User $user)
    {
        $validator = Validator::make($data, [
            'digital_signature' => 'required|mimes:jpeg,jpg,png,gif',
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        Storage::disk('public')->delete('user/'.$user->digital_signature);

        $filename = 'user_'.date('YmdHis').rand(0001, 9999).'.'.$ext;
        Storage::disk('public')->putFileAs('user', $data['digital_signature'], $filename);

        return $filename;
    }
}
