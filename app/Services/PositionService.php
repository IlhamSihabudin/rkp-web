<?php

namespace App\Services;

use App\Models\Role;
use App\Repositories\PositionRepository;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;
use Yajra\DataTables\DataTables;

class PositionService
{
    protected $positionRepository;

    public function __construct()
    {
        $this->positionRepository = new PositionRepository();
    }

    public function validation(array $data)
    {
        $validator = Validator::make($data, [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }
    }

    public function getDatatables()
    {
        $query = $this->positionRepository->getQueryAll();

        return DataTables::of($query)
            ->editColumn('action', function ($model){
                $response = "
                    <div class='text-center'>
                        <a href='".route('position.edit', $model['id'])."' class='btn btn-sm btn-light-success btn-circle btn-icon mr-2' title='Edit'><i class='fas fa-pen icon-nm'></i></a>
                    </div>";

                return $response;
            })
            ->make(true);
    }

    public function updateData($data, Role $position)
    {
        $this->validation($data);

        $this->positionRepository->update($data, $position);
    }
}
