<?php

namespace App\Services;

use App\Models\Department;
use App\Repositories\DepartmentRepository;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;
use Yajra\DataTables\DataTables;

class DepartmentService
{
    protected $departmentRepository;

    public function __construct()
    {
        $this->departmentRepository = new DepartmentRepository();
    }

    public function validation(array $data)
    {
        $validator = Validator::make($data, [
            'name' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }
    }

    public function getDatatables()
    {
        $query = $this->departmentRepository->getQueryAll();

        return DataTables::of($query)
            ->editColumn('action', function ($model){
                $response = "
                    <div class='text-center'>
                        <a href='".route('department.edit', $model['id'])."' class='btn btn-sm btn-light-success btn-circle btn-icon mr-2' title='Edit'><i class='fas fa-pen icon-nm'></i></a>
                        <a href='".route('department.destroy', $model['id'])."' class='btn btn-sm btn-light-danger btn-circle btn-icon mr-2 btn-delete' title='Delete'><i class='fas fa-trash-alt icon-nm'></i></a>
                    </div>";

                return $response;
            })
            ->make(true);
    }

    public function saveData(array $data)
    {
        $this->validation($data);

        $this->departmentRepository->insert($data);
    }

    public function updateData($data, Department $department)
    {
        $this->validation($data);

        $this->departmentRepository->update($data, $department);
    }
}
