<?php

namespace App\Services;

use App\Models\Document;
use App\Repositories\DocumentApprovalRepository;
use App\Repositories\DocumentAttachmentRepository;
use App\Repositories\DocumentDetailRepository;
use App\Repositories\DocumentRepository;
use App\Repositories\ItemRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;
use Ramsey\Uuid\Uuid;
use Yajra\DataTables\DataTables;

class DocumentService
{
    protected $documentRepository, $itemRepository,
        $documentDetailRepository, $documentAttachmentRepository,
        $documentApprovalRepository;

    public function __construct()
    {
        $this->documentRepository = new DocumentRepository();
        $this->itemRepository = new ItemRepository();
        $this->documentDetailRepository = new DocumentDetailRepository();
        $this->documentAttachmentRepository = new DocumentAttachmentRepository();
        $this->documentApprovalRepository = new DocumentApprovalRepository();
    }

    public function validation(array $data)
    {
        $validator = Validator::make($data, [
            'no_document' => 'required',
            'tgl_document' => 'required',
            'site' => 'required',
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }
    }

    public function getDatatables()
    {
        $query = $this->documentRepository->getQueryAll();

        return DataTables::of($query)
            ->editColumn('tgl_document', function ($model){
                return date('d F Y', strtotime($model['tgl_document']));
            })
            ->editColumn('action', function ($model){
                $response = "<div class='text-center'>";
                $response .= "<a href='".route('document.show', $model['id'])."' class='btn btn-sm btn-light-primary btn-circle btn-icon mr-2' title='Detail'><i class='fas fa-list icon-nm'></i></a>";

                if ($model['status'] == 3){
                    $response .= "<a href='".route('document.pdf', $model['id'])."' class='btn btn-sm btn-light-danger mr-2' title='Export PDF'><i class='far fa-file-pdf icon-nm'></i> Export PDF</a>";
                }

                $response .= "</div>";

                return $response;
            })
            ->make(true);
    }

    public function saveData(array $data)
    {
        $this->validation($data);

        $document = $this->documentRepository->insert([
            'no_document' => $data['no_document'],
            'tgl_document' => $data['tgl_document'],
            'site' => $data['site'],
            'status' => '1',
            'department_id' => Auth::user()->department_id,
        ]);

        if (!empty($data['items-list'])){
            $datas = [];
            foreach ($data['items-list'] as $item){
                $item_id = $this->itemRepository->checkItem($item['item']);
                $datas[] = [
                    'id' => Uuid::uuid4()->toString(),
                    'document_id' => $document->id,
                    'item_id' => $item_id,
                    'jumlah' => $item['jumlah'],
                    'harga' => $item['harga'],
                    'keterangan' => $item['keterangan']
                ];
            }

            $this->documentDetailRepository->multipleInsert($datas);
        }

        if (!empty($data['attachment-list'])){
            $datas = [];
            foreach ($data['attachment-list'] as $attachment){
                $filename = date('YmdHis') . '_' .$attachment['attachment']->getClientOriginalName();
                Storage::disk('public')->putFileAs('document-attachment', $attachment['attachment'], $filename);

                $datas[] = [
                    'id' => Uuid::uuid4()->toString(),
                    'document_id' => $document->id,
                    'file' => $filename,
                ];
            }

            $this->documentAttachmentRepository->multipleInsert($datas);
        }

        $this->documentApprovalRepository->insert([
            'document_id' => $document->id
        ]);
    }

    public function updateData($data, Document $document)
    {
        $this->validation($data);

        $this->documentRepository->update($data, $document);
    }

    public function approveDocument($data, Document $document)
    {
        foreach ($data['detail_id'] as $key => $detail_id){
            $this->documentDetailRepository->updateStatus($detail_id, $data['status_detail'][$key]);
        }

        foreach ($data['attach_id'] as $key => $attach_id){
            $this->documentAttachmentRepository->updateStatus($attach_id, $data['status_attach'][$key]);
        }

        $this->documentRepository->updateStatus($document);

        if (Auth::user()->hasRole('Sect Head')){
            $data = [
                'sect_head_id' => Auth::id(),
                'sect_head_tgl' => date('Y-m-d H:i:s')
            ];
        }elseif (Auth::user()->hasRole('GM')){
            $data = [
                'gm_id' => Auth::id(),
                'gm_tgl' => date('Y-m-d H:i:s')
            ];
        }else{
            $data = [];
        }

        $this->documentApprovalRepository->setApprove($document->id, $data);
    }
}
